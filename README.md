# Read Me

This tool will scan a mastodon server of your choosing, and start posting when
the federated URLs are owned by Meta. 

This works by IP address because changing your ranges is a hell of a lot harder
than changing your registrar. We do this robustly by checking both IPv6 and IPv4
where possible. However, sites that only report IPv4 addresses are solely checked
on ASN. I find this acceptable as Facebook/Meta doesn't tend to host anything 
other than it's own stuff. 

Make sure you put your access_token in 'token.secret' and change the URL on line
12 of 'metablock.py' to your instance URL. 


## Installation
1. Set up an application on your Mastodon instance
1. Clone this Repo
1. Copy your application's Access Token to the secrets.token file
1. Install the requirements (requirements.txt)
1. Run this script (metablock.py <url to instance>)

At this point you can either run it manually or schedule it with your scheduler
of choice. 

## Operation
Example with defaults: 

metablock.py --asn IPASN.DAT --domains domains.txt \
    --access_token <accessToken> https://mastodon.instance


# License
Licensed under the CNPL v7 found at https://thufie.lain.haus/files/CNPLv7.md