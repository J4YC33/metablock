from mastodon import Mastodon
import dns
import dns.resolver
import pyasn
import re
import argparse

# checkIPv4 checks a domain's IP address against a given ASN
def checkIPv4(domain, asn):
    result4 = dns.resolver.resolve(domain, 'A')
    search = re.search('(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)', str(result4.response.answer))
    ipv4 = search(0)
    if asndb.lookup(str(ipv4))[0] == asn: 
        return True
    else:
        return False
    
# checkIPv6 checks to see if a domain's IPv6 address uses Meta's range. 
# It's... bespoke. 
def checkIPv6(domain):
    # Note: this method only works for Meta. 
    # I'm sure I could probably do this some other way as well...
    result = dns.resolver.resolve(domain, 'AAAA')
    if str(result.response).find("face:b00c") > 0:
        return True
    else:
        return False
    
parser = argparse.ArgumentParser()

parser.add_argument("--asn", help="Location of an ASN database. Default: IPASN.DAT", default='IPASN.DAT')
parser.add_argument("--access_token", help="Access token for your Mastodon application. Default: tokens.secret", default='tokens.secret')
parser.add_argument("--domains", help="A file with a list of domains to check, 1 domain per line. Default: domains.txt", default='domains.txt')
parser.add_argument("url", help="The URL of your Mastodon instance")

args = parser.parse_args()

asndb = pyasn.pyasn(args.asn)

mastodon = Mastodon(
    access_token = args.access_token,
    api_base_url = args.url
)

try:
    metaDomains = open('Meta.txt','r').readlines()
except:
    metaDomains = []

knownDomains = []
for item in metaDomains:
    knownDomains.append(item.strip())

with open('Meta.txt','a') as output:
    domains = mastodon.instance_peers()
    domains = []
    try:
        with open(args.domains, 'r') as prepends:
            for entry in prepends: 
                if len(entry) > 0:
                    domains.insert(0,entry.strip())
    except:
        print("No domains file provided. Continuing.")

    for domain in domains:
        try:
            if (not (domain in knownDomains)) and checkIPv4(domain, 32934) and checkIPv6(domain):
                mastodon.status_post(domain + " is a Meta Domain. #Fediblock #Metablock\nThe complete list of tracked domains can be found at\ 
                                                https://gitlab.com/J4YC33/metablock/-/blob/main/Meta.txt")
                output.write(domain + '\n')
        except:
            if (not (domain in knownDomains)) and checkIPv4(domain, 32934):
                mastodon.status_post(domain + " is a Meta Domain. #Fediblock #Metablock\nThe complete list of tracked domains can be found at \
                                                https://gitlab.com/J4YC33/metablock/blobl/main/Meta.txt")
                output.write(domain + '\n')

    

                
        
